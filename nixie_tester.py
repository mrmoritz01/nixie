import i2c_nixie
import smbus
import sys
import time

bus = smbus.SMBus(1)

address = int(sys.argv[1])
sleeptime = 2
print("check nixie board on address " + hex(address))

test_tube = i2c_nixie.i2cNixie(bus, address)
test_tube.immediate_update = True

colors = {
    "RED" :     "R",
    "GREEN" :   "G",
    "BLUE" :    "B",
    "YELLOW" :  "RG",
    "PINK" :    "RB",
    "CYAN" :    "GB",
    "WHITE":    "RGB"
}

for c in colors.keys():
    print(c)
    test_tube.set_rgb(colors[c])
    time.sleep(sleeptime)

for d in range(10):
    print(str(d))
    test_tube.set_digit(str(d))
    time.sleep(sleeptime)

print("off")
test_tube.set_digit("x")
time.sleep(sleeptime)

print("Left Dot")
test_tube.set_dot(True, False)
time.sleep(sleeptime)

print("Right Dot")
test_tube.set_dot(False, True)
time.sleep(sleeptime)

print("Both Dots")
test_tube.set_dot(True, True)
time.sleep(sleeptime)

print("Done.")

