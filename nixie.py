from datetime import datetime
import time
import sys
import argparse
import nixie_clock
import http.server
import json
from urllib.parse import urlsplit
import random

# This class will handles any incoming request from
# the browser
class nixieHttpHandler(http.server.BaseHTTPRequestHandler):

    pin = 0
    pin_expires = datetime.now()
    logged_in = False

    def error_resp(self, error_str = "OK"):
        data = {}
        data["error"] = error_str

        return json.dumps(data)

    def get_nixie_json(self, path):
        ret = None

        if path == "/api/nixie/states.json":
            data={}

            data["nixies"] = []

            i_nixie = 0
            for n in nixie_clk.pwm_nixies:
                if n is not None:
                    data["nixies"].append({
                        "idx" : str(i_nixie),
                        "current_digit" : n.nixie_digit,
                        "left_dot":     str(n.nixie_dot_left),
                        "right_dot":    str(n.nixie_dot_left),
                        "second_dot":   str(i_nixie == nixie_clk.sec_dot_nixie),
                        "pwm_ratio_digit" : str(n.pwm_ratio[0]),
                        "pwm_ratio_R": str(n.pwm_ratio[1]),
                        "pwm_ratio_G": str(n.pwm_ratio[2]),
                        "pwm_ratio_B": str(n.pwm_ratio[2])
                    })
                i_nixie += 1

            ret = json.dumps(data)

        if "/api/nixie/set/rgb.json" in path:
            error = "ERR_FAIL"
            try:
                param_tuples = urlsplit(path).query.split("&")

                if "tube" in param_tuples:
                    for p in param_tuples:
                        [name, val] = p.split("=")
                        if "tube" == name:
                            tube = int(val)
                            #Todo: Set RGB for each Tube seperately

                for p in param_tuples:
                    [name, val] = p.split("=")
                    if "color" == name:
                        #set color to nixie
                        nixie_clk.set_led_color(parse_hex_rgb_to_float(val))
                        error = "OK"

            except:
                error = "ERR_URLPARSE"

            ret = self.error_resp(error)

        if "/api/nixie/pin.json" in path:
            error = "ERR_FAIL"
            try:
                random.seed()
                pin = random.randint(0,9999)
                nixie_clk.show_pin(pin)
            except:
                error = "ERR_URLPARSE"

            ret = self.error_resp(error)

        if "/api/nixie/pin_ack.json" in path:
            error = "ERR_FAIL"
            try:
                nixie_clk.ack_pin()
            except:
                error = "ERR_URLPARSE"

            ret = self.error_resp(error)

        if "/api/nixie/set/brightness.json" in path:
            error = "ERR_FAIL"
            try:
                param_tuples = urlsplit(path).query.split("&")

                if "tube" in param_tuples:
                    for p in param_tuples:
                        [name, val] = p.split("=")
                        if "tube" == name:
                            tube = int(val)
                            # Todo: Set Brightness for each Tube seperately

                for p in param_tuples:
                    [name, val] = p.split("=")
                    if "val" == name:
                        # set brightness to nixie
                        nixie_clk.set_brightness(float(val))
                        error = "OK"

            except:
                error = "ERR_URLPARSE"

            ret = self.error_resp(error)

        return ret



    # Handler for the GET requests
    def do_GET(self):
        print(self.path)
        resp_str = self.get_nixie_json(self.path)

        if resp_str is not None:
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            # Send the html message

            self.wfile.write(bytes(resp_str, encoding="ascii"))
        else:
            self.send_error(404, "not found")
        return


from i2c_nixie import i2cNixie, nixie_pwm

bus = None

def parse_hex_rgb_to_float(hex_col_str):
        hex_col_str = hex_col_str.strip("#")
        rgb_float = [0.0, 0.0, 0.0]
        try:
                for i in range(3):
                        rgb_float[i] = 100.0 * float(int(hex_col_str[(i*2):(i*2)+2], 16)) / 255.0

        except:
                msg = hex_col_str + " is not a valid hex color string."
                raise argparse.ArgumentTypeError(msg)
        return rgb_float


parser = argparse.ArgumentParser(description='I2C Nixie Control')
parser.add_argument('--color', dest='color', type=parse_hex_rgb_to_float,
                    help='LED Color for all nixies in Hex notation e.g. #FF0000 for RED or #cc99ff for purple, see https://www.w3schools.com/colors/colors_picker.asp')

parser.add_argument('--brightness', dest='brightness', type=float,
                    help='Nixie Digit brightness (0 .. 100) in percent')


parser.add_argument('--testmode', dest='testmode', action="store_true",
                    help='LED Color for all nixies in Hex notation e.g. #FF0000 for RED or #cc99ff for purple, see https://www.w3schools.com/colors/colors_picker.asp')

parser.add_argument('--bus', dest='bus', type=int, action="store", default=1,
                    help='I2C But to be used, usually 1')

parser.add_argument('--web', dest='web', action="store_true", default=False,
                    help='start webserver')


args = parser.parse_args()

if not args.testmode:
    import smbus
    bus = smbus.SMBus(args.bus)

#configure nixie
nixie_clk = nixie_clock.nixieClock()

nixie_clk.init_nixie("H", 0x24, bus)
nixie_clk.init_nixie("h", 0x25, bus)
nixie_clk.init_nixie("M", 0x26, bus, sec_dot=True)
nixie_clk.init_nixie("m", 0x27, bus)

nixie_clk.set_brightness(args.brightness)
nixie_clk.set_led_color(args.color)

#start nixie service
clk_handle = nixie_clk.nixie_clock_run(testmode=args.testmode)

if args.web:
    #start nixie webserver
    PORT_NUMBER = 9000
    try:
        # Create a web server and define the handler to manage the
        # incoming request
        server = http.server.HTTPServer(('', PORT_NUMBER), nixieHttpHandler)
        print('Started httpserver on port ' + str(PORT_NUMBER))

        # Wait forever for incoming htto requests
        server.serve_forever()

    except KeyboardInterrupt:
        print('^C received, shutting down the web server')
        server.socket.close()
        clk_handle.kill()

#wait for nixie service finish (should never happen)
clk_handle.join()
