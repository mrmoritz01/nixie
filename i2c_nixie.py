from pca import pca95xx


class i2cNixie:

        digit_lookup = {
                "0":      0o10,
                "1":      0o7,
                "2":      0o16,
                "3":      0o4,
                "4":      0o3,
                "5":      0o5,
                "6":      0o0,
                "7":      0o6,
                "8":      0o15,
                "9":      0o13,
                "LDot": 0o14,
                "RDot": 0o2,
                "R":    0o12,
                "G":    0o17,
                "B":    0o1
        }

        color_lookup = {
                "off": "",
                "red": "R",
                "green": "G",
                "blue": "B",
                "yellow": "RG",
                "cyan": "GB",
                "pink": "RB",
                "white": "RGB"
        }

        dot_mask = (1 << digit_lookup["LDot"]) + (1 << digit_lookup["RDot"])
        rgb_mask = (1 << digit_lookup["R"]) + (1 << digit_lookup["G"]) + (1 << digit_lookup["B"])

        pca = None
        current_digit = "0"
        left_dot = False
        right_dot = False
        RGB = "off"
        out_val = 0
        immediate_update = False

        def __init__(self, bus, address):
                self.pca = pca95xx(bus, address)
                self.set_digit(self.current_digit)
                self.set_rgb(self.RGB)
                self.set_dot(self.left_dot, self.right_dot)
                if False == self.immediate_update:
                        self.update_tube()
                self.pca.init_outputs()

        def set_digit(self, new_digit):
                if True == self.immediate_update:
                        self.out_val = self.pca.get_output_value()

                self.out_val &= (self.dot_mask | self.rgb_mask)

                if "x" != new_digit: # "x" means off
                        self.out_val |= (1 << self.digit_lookup[new_digit])

                self.current_digit = new_digit

                if True == self.immediate_update:
                        self.pca.set_output_value(self.out_val)

        def set_color(self, color):

                try:
                        rgb = self.color_lookup[color]
                except:
                        rgb = ""

                self.set_rgb(rgb)

        def set_rgb(self, RGB):
                if True == self.immediate_update:
                        out_val = self.pca.get_output_value()

                self.out_val |= self.rgb_mask

                #LED switching is inverted
                if "R" in RGB:
                        self.out_val &= ~(1 << self.digit_lookup["R"])

                if "B" in RGB:
                        self.out_val &= ~(1 << self.digit_lookup["B"])

                if "G" in RGB:
                        self.out_val &= ~(1 << self.digit_lookup["G"])

                self.RGB = RGB

                if True == self.immediate_update:
                        self.pca.set_output_value(self.out_val)

        def set_dot(self, Left, Right):
                if True == self.immediate_update:
                        self.out_val = self.pca.get_output_value()

                self.out_val &= ~self.dot_mask

                if Left:
                        self.out_val |= (1 << self.digit_lookup["LDot"])

                if Right:
                        self.out_val |= (1 << self.digit_lookup["RDot"])

                self.left_dot = Left
                self.right_dot = Right

                if True == self.immediate_update:
                        self.pca.set_output_value(self.out_val)

        def update_tube(self):
                self.pca.set_output_value(self.out_val)
                self.out_val = 0 #init for next cycle





class nixie_pwm:
        nixie = None

        PWM_DIGIT = 0
        PWM_R = 1
        PWM_G = 2
        PWM_B = 3

        pwm_ratio = [50.0, 50.0, 50.0, 50.0]

        pwm_lookup_len = 24
        pwm_lookup_pos = 0

        pwm_onoff_lookup_digit = []
        pwm_onoff_lookup_R = []
        pwm_onoff_lookup_G = []
        pwm_onoff_lookup_B = []

        nixie_digit = "x"
        nixie_dot_left = False
        nixie_dot_right = False



        def __init__(self, nixie):
                self.nixie = nixie

        def set_digit(self, digit):
                self.nixie_digit = digit

        def set_dot(self, left_dot = False, right_dot = False):
                self.nixie_dot_left = left_dot
                self.nixie_dot_right = right_dot

        def set_pwm_ratio(self, ratio, target):

                if self.PWM_DIGIT != target:
                    #apply overall brightness
                    self.pwm_ratio[target] = ratio * (self.pwm_ratio[0] / 100.0)
                else:
                    self.pwm_ratio[target] = ratio

                _pwm_onoff_lookup  = [1] * int((self.pwm_ratio[target]/100.0) * self.pwm_lookup_len)        #on-time
                _pwm_onoff_lookup += [0] * int(self.pwm_lookup_len - len(_pwm_onoff_lookup))    #off-time

                if self.PWM_DIGIT == target:
                        self.pwm_onoff_lookup_digit = _pwm_onoff_lookup


                if self.PWM_R == target:
                        self.pwm_onoff_lookup_R = _pwm_onoff_lookup


                if self.PWM_G == target:
                        self.pwm_onoff_lookup_G = _pwm_onoff_lookup


                if self.PWM_B == target:
                        self.pwm_onoff_lookup_B = _pwm_onoff_lookup



        def tick(self):
                if 1 == self.pwm_onoff_lookup_digit[self.pwm_lookup_pos]:
                        self.nixie.set_digit(self.nixie_digit)
                        self.nixie.set_dot(self.nixie_dot_left, self.nixie_dot_right)

                rgb_str = ""
                if 1 == self.pwm_onoff_lookup_R[self.pwm_lookup_pos]:
                        rgb_str += "R"

                if 1 == self.pwm_onoff_lookup_G[self.pwm_lookup_pos]:
                        rgb_str += "G"

                if 1 == self.pwm_onoff_lookup_B[self.pwm_lookup_pos]:
                        rgb_str += "B"

                self.nixie.set_rgb(rgb_str)
                self.nixie.update_tube()

                self.pwm_lookup_pos += 1
                if self.pwm_lookup_pos >= self.pwm_lookup_len:
                        self.pwm_lookup_pos = 0