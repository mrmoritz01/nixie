#include "i2c_nixie.h"
#include "pca95xx.h"

//Lookup Table for nixie3 i2c board
byte digit_lookup(char digit)
{
  uint16_t ret = 0;

  //this lookup shows which digit is connected to which pin
  //Attention - these are octal numbers to match the pin naming
  switch(digit)
  {
  case '0':      ret = 1 << 010; break;
  case '1':      ret = 1 << 07;  break;
  case '2':      ret = 1 << 016; break;
  case '3':      ret = 1 << 04;  break;
  case '4':      ret = 1 << 03;  break;
  case '5':      ret = 1 << 05;  break;
  case '6':      ret = 1 << 00;  break;
  case '7':      ret = 1 << 06;  break;
  case '8':      ret = 1 << 015; break;
  case '9':      ret = 1 << 013; break;
  case 'l':      ret = 1 << 014; break;
  case 'r':      ret = 1 << 02;  break;
  case 'R':      ret = 1 << 012; break;
  case 'G':      ret = 1 << 017; break;
  case 'B':      ret = 1 << 01;  break;
  default: break;  
  }

  return ret;
}

i2cNixie::i2cNixie(int pin_sda, int pin_scl, byte addr)
{
  this->pca.setup(pin_sda, pin_scl, addr);

  this->digit = '0';
  this->dot_l = false;
  this->dot_r = false;
  this->r = 100;
  this->g = 100;
  this->b = 100;
  this->digit_brightness = 100;
  this->cycle_cnt = 0;

  this->refresh();
  this->pca.config_outputs(0xFFFF);
}

void i2cNixie::set_digit(char digit, bool dot_l, bool dot_r, byte brightness)
{
    this->out_digit = digit_lookup(digit);
    if (dot_l)
    {
      this->out_digit += digit_lookup('l');
    }
    
    if (dot_l)
    {
      this->out_digit += digit_lookup('r');
    }

    this->digit_brightness = 100;
}

void i2cNixie::set_color(int r, int g, int b, int brightness)
{
    this->r = (r*brightness) / 100;
    this->g = (g*brightness) / 100;
    this->b = (b*brightness) / 100;;
}


void i2cNixie::refresh()
{
    this->cycle_cnt++;
    if( this->cycle_cnt >= MAX_CYCLES )
    {
        this->cycle_cnt = 0;
    }

    uint16_t out_val = 0;

    byte cycles_percent = ((100*this->cycle_cnt)/MAX_CYCLES);

    if( cycles_percent < this->digit_brightness )
    {
        out_val += this->out_digit;
    }

    if( cycles_percent < this->r )
    {
        out_val += digit_lookup('R');
    }

    if( cycles_percent < this->g )
    {
        out_val += digit_lookup('G');
    }

    if( cycles_percent < this->b )
    {
        out_val += digit_lookup('B');
    }

    this->pca.set_outputs((int)out_val);
    
}
