#include "pca95xx.h"
#include "Wire.h"


PCA95xx::PCA95xx()
{
 
  this->current_out_L = 0;
  this->current_out_H = 0;

 

Wire.begin(this->pin_sda, this->pin_scl);
}

void PCA95xx::setup(int pin_sda, int pin_scl, byte addr)
{
   this->pin_sda = pin_sda;
  this->pin_scl = pin_scl;
  this->addr = addr;
   Serial.printf("PCA95xx: SDA=%d SCL=%d\n", this->pin_sda, this->pin_scl, this->addr);
}

void PCA95xx::config_outputs(int pin_mask_out)
{
    Serial.printf("Writing pin_mask 0x%04X to addr=0x%02X\n", pin_mask_out, this->addr);
Wire.beginTransmission(this->addr);
    Wire.write(PCA95XX_REG_CFG_0);
    Wire.write( (byte) ( ((~pin_mask_out)>>0) & 0xFF) );
    Serial.printf("written %02X to reg %d\n", (byte) ( ((~pin_mask_out)>>0) & 0xFF), PCA95XX_REG_CFG_0);
    //Wire.write(PCA95XX_REG_CFG_1);
    Wire.write( (byte) ( ((~pin_mask_out)>>8) & 0xFF) );
    Serial.printf("written %02X to reg %d\n", (byte) ( ((~pin_mask_out)>>8) & 0xFF), PCA95XX_REG_CFG_1);
Wire.endTransmission();
}



uint16_t PCA95xx::get_inputs()
{
  
}



void PCA95xx::set_outputs(int out_value)
{
    byte out_L = (byte) ( ((out_value)>>0) & 0xFF);
    byte out_H = (byte) ( ((out_value)>>8) & 0xFF);

    bool byte_L_changed = out_L != this->current_out_L;
    bool byte_H_changed = out_H != this->current_out_H;

    //send changed bytes only to save time for software PWM
  
Wire.beginTransmission(this->addr);
    if(byte_L_changed)
    {
      Wire.write(PCA95XX_REG_OUT_0);
      Wire.write( out_L );
      if(byte_H_changed)
      {
        Wire.write( out_H );
      }
    }
    else if(byte_H_changed)
    {
      Wire.write(PCA95XX_REG_OUT_1);
      Wire.write( out_H );
    }
    else
    {
      //nothing changed
    }
    
Wire.endTransmission();
}
