#ifndef _I2C_NIXIE_H_
#define _I2C_NIXIE_H_

#include "pca95xx.h"

#define MAX_CYCLES 10

class i2cNixie
{   
    private:
        PCA95xx pca;
        char digit;
        bool dot_l;
        bool dot_r;
        byte r;
        byte g;
        byte b;
        byte rgb_brightness;
        byte digit_brightness;
        uint16_t out_digit;
        uint16_t out_rgb;
        byte cycle_cnt;
    
    public:
        i2cNixie(int pin_sda, int pin_scl, byte addr);
        void set_digit(char digit, bool dot_l = false, bool dot_r = false, byte brightness = 100);
        void set_color(int r, int g, int b, int brightness = 100);      
        void refresh();
};

#endif
