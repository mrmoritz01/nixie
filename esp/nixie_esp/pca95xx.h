#ifndef _PCA95xx_H_
#define _PCA95xx_H_

#include "Arduino.h"

typedef enum pca95xx_register_e_tag
{
  PCA95XX_REG_IN_0 = 0,
  PCA95XX_REG_IN_1,
  PCA95XX_REG_OUT_0,
  PCA95XX_REG_OUT_1,
  PCA95XX_REG_POL_INV_0,
  PCA95XX_REG_POL_INV_1,
  PCA95XX_REG_CFG_0,
  PCA95XX_REG_CFG_1
} pca95xx_register_e;

class PCA95xx
{  
    private:
        int pin_sda;
        int pin_scl;
        byte addr;
        byte current_out_L;
        byte current_out_H;
    
    public:
        PCA95xx();
        void setup(int pin_sda, int pin_scl, byte addr);
        void config_outputs(int pin_mask_out);
        void set_outputs(int out_value);
        uint16_t get_inputs();
};

#endif
