#include "Wire.h"
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include "pca95xx.h"

WiFiManager wifiManager;

void setup(){
  Serial.begin(115200); 
  while(!Serial){} // Waiting for serial connection
 
  Serial.println();
  Serial.println("Start I2C scanner ...");
  Serial.print("\r\n");

 // wifiManager.autoConnect("NixieAP", "12345678");
  
  byte count = 0;

  #define PIN_SDA 4
  #define PIN_SCL 5

  Wire.setClock(100000);
  Wire.begin(PIN_SDA, PIN_SCL);
  for (byte i = 8; i < 120; i++)
  {
    Wire.beginTransmission(i);
    if (Wire.endTransmission() == 0)
      {
      Serial.print("Found I2C Device: ");
      Serial.print(" (0x");
      Serial.print(i, HEX);
      Serial.println(")");
      count++;
      delay(1);
      }
  }
  Serial.print("\r\n");
  Serial.println("Finish I2C scanner");
  Serial.print("Found ");
  Serial.print(count, HEX);
  Serial.println(" Device(s).");

  for( int reg = 0; reg < 8; reg++)
  {
     Wire.beginTransmission(0x27);  
     Wire.write(reg);  // set register for read
     Wire.endTransmission(false); // false to not release the line
  
     Wire.requestFrom(0x27,1); // request bytes from register XY
     byte buff = Wire.read();
     
       Serial.print(reg);
       Serial.print(":");
       Serial.print(buff, HEX);
       Serial.println();
  }

  PCA95xx pca;
  pca.setup(PIN_SDA, PIN_SCL, 0x27);
  pca.config_outputs(0xFFFF);
  pca.set_outputs(0x1234);

  for( int reg = 0; reg < 8; reg++)
  {
     Wire.beginTransmission(0x27);  
     Wire.write(reg);  // set register for read
     Wire.endTransmission(false); // false to not release the line
  
     Wire.requestFrom(0x27,1); // request bytes from register XY
     byte buff = Wire.read();
     
       Serial.print(reg);
       Serial.print(":");
       Serial.print(buff, HEX);
       Serial.println();
  }

  
}

void loop() {}
