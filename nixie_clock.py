import threading
import datetime
import time
import i2c_nixie


class nixieClock:

    i2c_nixies = [None, None, None, None, None]
    pwm_nixies = [None, None, None, None, None]
    sec_dot_nixie = 0
    pwm_time = 0.0
    brightness = 0.0
    show_pin = False
    pin_str = ""
    testmode = False

    def init_nixie(self, pos, addr, bus, sec_dot = False):
        if "H" == pos:
            idx = 0
        if "h" == pos:
            idx = 1
        if "M" == pos:
            idx = 3
        if "m" == pos:
            idx = 4

        if sec_dot:
            self.sec_dot_nixie = idx

        self.i2c_nixies[idx] = i2c_nixie.i2cNixie(bus, addr)
        self.pwm_nixies[idx] = i2c_nixie.nixie_pwm(self.i2c_nixies[idx])

    def set_pwm_base_freq(self, pwm_freq_Hz):
        self.pwm_time = 1.0 / (float(pwm_freq_Hz) * i2c_nixie.nixie_pwm.pwm_lookup_len)

    def set_led_color(self, color_float):
        for p in self.pwm_nixies:
            if p is not None:
                for i in range(len(color_float)):
                    p.set_pwm_ratio(color_float[i], p.PWM_R + i)

    def set_brightness(self, brightness):
        self.brightness = brightness

        for p in self.pwm_nixies:
            if p is not None:
                    p.set_pwm_ratio(brightness, p.PWM_DIGIT)

    def show_pin(self, pin):
        self.pin_str = str(pin).zfill(4)
        self.show_pin = True

    def ack_pin(self):
        self.show_pin = False
        self.pin_str = ""

    def nixie_task(self):
        prev_date_str = "         "
        while True:

            date_str = str(datetime.datetime.now().time())[0:8]

            if True: #False == self.show_pin:
                # print(date_str)
                if date_str != prev_date_str:
                    for i in range(5):
                        if date_str[i] != prev_date_str[i]:
                            if None != self.pwm_nixies[i]:
                                # nixie_tubes[i].set_digit(date_str[i])
                                self.pwm_nixies[i].set_digit(date_str[i])

                    if 0 != (int(date_str[6:8]) & 0x01):
                        self.pwm_nixies[self.sec_dot_nixie].set_dot(False, True)
                        # nixie_tubes[sec_dot_nixie].set_dot(False, Right=True)
                    else:
                        self.pwm_nixies[self.sec_dot_nixie].set_dot(False, False)

                    prev_date_str = date_str
            else:
                #show pin
                i_nixie = [0,1,3,4]

                for i_pin in range(4):
                    try:
                        if self.pwm_nixies[i_nixie[i_pin]] is not None:
                            self.pwm_nixies[i_nixie[i_pin]].set_digit(self.pin_str[i_pin])
                    except:
                        print("error")

            # Tick nixie PWM
            for pwm in self.pwm_nixies:
                if pwm is not None:
                    pwm.tick()
            time.sleep(self.pwm_time)
            if self.testmode:
                time.sleep(1.0) #1 second sleep in testmode (PC-only)


    def nixie_clock_run(self, testmode=False):
        self.testmode = testmode
        t = threading.Thread(target=self.nixie_task)
        t.start()
        return t
