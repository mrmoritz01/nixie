import time


class pca8575:
        address = 0
        output_value = 0
        next_byte = 0
        bus = None

        def get_output_value(self):
                return self.output_value

        def set_output_value(self, new_value):
                self.output_value = new_value

                print("new_value=" + hex(new_value))

                for i_byte in range(2):
                        try:
                                if None != self.bus:
                                        self.bus.write_byte_data(self.address, (self.output_value >> (8*self.next_byte)) & 0xFF)
                                else:
                                     print(hex((self.output_value >> (8*self.next_byte)) & 0xFF))
                                self.next_byte ^= 1
                        except:
                                self.next_byte = self.next_byte

                print("----------")

        def __init__(self, bus, address):
                self.bus = bus
                self.address = address

class pca95xx:
        address = 0
        output_value = 0
        bus = None
        print_regs = False
        slp = 0#0.001
        #lock = allocate_lock()

        commands = {
                "input_p0":     0,
                "input_p1":     1,
                "output_p0":    2,
                "output_p1":    3,
                "polarity_inv_p0": 4,
                "polarity_inv_p1": 5,
                "cfg_p0":       6,
                "cfg_p1":       7
        }

        def get_output_value(self):
                return self.output_value


        def set_output_value(self, new_value):

                byte_0_changed = ((new_value & 0xFF) != (self.output_value & 0xFF))
                byte_1_changed = ((new_value & 0xFF00) != (self.output_value & 0xFF00))

                old_value = self.output_value
                self.output_value = new_value

                if None != self.bus:
                    try:
                        if byte_0_changed:
                                self.bus.write_byte_data(self.address, self.commands["output_p0"], (self.output_value >> 0) & 0xFF)
                                if 0 != self.slp:
                                        time.sleep(self.slp)

                        if byte_1_changed:
                                self.bus.write_byte_data(self.address, self.commands["output_p1"], (self.output_value >> 8) & 0xFF)
                                if 0 != self.slp:
                                        time.sleep(self.slp)
                    except:
                        self.output_value=old_value

                if self.print_regs:
                        print("a=" + hex(self.address) + " r=" + hex(self.commands["output_p0"]) + " v=" + hex( (self.output_value >> 0) & 0xFF))
                        print("a=" + hex(self.address) + " r=" + hex(self.commands["output_p1"]) + " v=" + hex((self.output_value >> 8) & 0xFF))

        def init_outputs(self, output_mask=0x0000):
                if None != self.bus:
                    try:
                        self.bus.write_byte_data(self.address, self.commands["cfg_p0"], (output_mask >> 0) & 0xFF)
                        if 0 != self.slp:
                                time.sleep(self.slp)
                        self.bus.write_byte_data(self.address, self.commands["cfg_p1"], (output_mask >> 8) & 0xFF)
                        if 0 != self.slp:
                                time.sleep(self.slp)
                    except:
                        pass

                if self.print_regs:
                        print("a=" + hex(self.address) + " r=" + hex(self.commands["cfg_p0"]) + " v=" + hex( (output_mask>> 0) & 0xFF))
                        print("a=" + hex(self.address) + " r=" + hex(self.commands["cfg_p1"]) + " v=" + hex((output_mask >> 8) & 0xFF))

        def __init__(self, bus, address, Print_regs=False):
                self.bus = bus
                self.address = address
                self.print_regs = Print_regs

